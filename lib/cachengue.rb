# frozen_string_literal: true

require 'cachengue/caching'
require 'cachengue/namespace'
require 'cachengue/proxy'
require 'cachengue/version'

module Cachengue
  module ActiveRecord
    extend ActiveSupport::Concern

    included do
      extend Cachengue::Namespace

      before_save { self.class.cachengue_clear }
    end
  end
end
