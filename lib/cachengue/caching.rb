# frozen_string_literal: true

module Cachengue
  module Caching
    module_function

    def format_key_by_options(namespace, method_name, args, options)
      format_key(
        options[:namespace] || namespace,
        options[:key] || options[:method] || method_name,
        options[:args] || args,
        options[:args_hash]
      )
    end

    def format_key(namespace, method_name, args, args_hash)
      args = args.is_a?(Array) ? args.map(&:as_json) : [args.as_json]
      args = Digest::SHA256.base64digest(String(args)) if args_hash
      namespace = key_prefix(namespace)

      "#{namespace}:#{method_name}:#{args}"
    end

    def format_options(options)
      options.without(
        :args,
        :key,
        :args_hash,
        :method,
        :multi,
        :namespace
      )
    end

    def key_prefix(namespace = nil)
      if namespace.present?
        "cachengue:#{namespace.to_s.underscore}"
      else
        'cachengue'
      end
    end

    # ':*' match with all keys in Redis.
    def key_suffix(strategy)
      strategy == :all ? ':*' : ''
    end

    def keys(namespace = nil, strategy = :all)
      prefix = key_prefix(namespace)
      suffix = key_suffix(strategy)

      Rails.cache.redis.keys("#{prefix}#{suffix}")
    end

    def clear(namespace = nil, strategy = :all)
      prefix = key_prefix(namespace)
      suffix = key_suffix(strategy)

      Rails.cache.delete_matched("#{prefix}#{suffix}").count
    end

    def clear_by(options)
      key = format_key(
        options.fetch(:namespace),
        options[:key] || options.fetch(:method),
        options.fetch(:args),
        options[:args_hash]
      )

      Rails.cache.delete(key)
    end

    def get_value_by(args)
      value = yield(*args)

      if defined?(::ActiveRecord) && value.is_a?(::ActiveRecord::Relation)
        value.to_a
      else
        value
      end
    end

    def fetch(namespace, method_name, args, options = {}, &block)
      if options[:multi]
        fetch_multi(namespace, method_name, args, options, &block)
      else
        fetch_one(namespace, method_name, args, options, &block)
      end
    end

    def fetch_one(namespace, method_name, args, options = {}, &block)
      key = format_key_by_options(namespace, method_name, args, options)
      options = format_options(options)

      Rails.cache.fetch(key, options) do
        get_value_by(args, &block)
      end
    end

    def fetch_multi(namespace, method_name, args_list, options = {}, &block)
      return {} if args_list.empty?

      args_keys = {}

      args_list.each do |args|
        key = format_key_by_options(namespace, method_name, args, options)

        args_keys[key] = args
      end

      options = format_options(options)
      results = read_multi(args_keys.keys)

      computed_keys = args_keys.keys - results.keys
      computed_values = computed_keys.map { |key| args_keys.fetch(key) }
      computed = write_multi(computed_keys, computed_values, options, &block)

      results.merge!(computed)
      results.transform_keys! { |key| args_keys.fetch(key) }
      results
    end

    def read_multi(keys)
      Rails.cache.read_multi(*keys)
    end

    def write_multi(keys, values, options, &block)
      results = {}

      keys.each_with_index do |key, index|
        results[key] = get_value_by(values.fetch(index), &block)
      end

      Rails.cache.write_multi(results, options)

      results
    end
  end
end
