# frozen_string_literal: true

# rubocop:disable Style/MethodMissingSuper

module Cachengue
  class Proxy
    def initialize(classx, options = {})
      @classx = classx
      @options = options
    end

    def cache_keys
      Cachengue::Caching.keys(@classx)
    end

    def cache_clear
      Cachengue::Caching.clear(@classx)
    end

    private

    def respond_to_missing?(method_name, include_all = false)
      @classx.respond_to?(method_name, include_all) || super
    end

    def method_missing(method_name, *args)
      Cachengue::Caching.fetch(@classx, method_name, args, @options) do
        @classx.public_send(method_name, *args)
      end
    end
  end
end
