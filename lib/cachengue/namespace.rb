# frozen_string_literal: true

module Cachengue
  module Namespace
    def cachengue(options = {}, &block)
      if block_given?
        Caching.fetch(self, nil, [], options, &block)
      else
        Proxy.new(self, options)
      end
    end

    def module_cachengue(method_name, options = {}, &block)
      singleton_class.instance_eval do
        define_method(method_name) do |*args|
          Caching.fetch_one(self, method_name, args, options, &block)
        end
      end

      return unless options[:multi]

      singleton_class.instance_eval do
        define_method("#{method_name}_multi") do |*args|
          Caching.fetch_multi(self, method_name, args, options, &block)
        end
      end
    end

    def cachengue_namespace(namespace, strategy: :all)
      __cachengue_namespaces__[namespace.to_sym] = strategy
    end

    def cachengue_clear_by(options)
      Caching.clear_by(
        options.reverse_merge(namespace: self)
      )
    end

    def cachengue_clear
      __cachengue_namespaces__.inject({}) do |results, (namespace, strategy)|
        results.merge!(
          namespace => Caching.clear(namespace, strategy)
        )
      end
    end

    private

    def __cachengue_namespaces__
      if class_variable_defined?(:@@cachengue_namespaces)
        class_variable_set(
          :@@cachengue_namespaces,
          class_variable_get(:@@cachengue_namespaces)
        )
      else
        class_variable_set(
          :@@cachengue_namespaces,
          to_s.to_sym => :all
        )
      end
    end
  end
end
