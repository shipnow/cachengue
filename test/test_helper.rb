require 'minitest/autorun'
require 'minitest/spec'
require 'cachengue/caching'
require 'cachengue/proxy'
require 'active_support/all'

class DummyClass
  extend Cachengue::Caching

  def self.posts
    [1, 2, 3, 4]
  end
end

class Rails
  def self.cache
    Cache.new
  end
end

class Cache
  def fetch(params, &new_block)
    params
  end
end
