# frozen_string_literal: true

require 'test_helper'

describe Cachengue do
  it 'returns cache' do
    DummyClass.cachengue.posts.wont_be_nil
  end
end
